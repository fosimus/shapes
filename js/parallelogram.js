import { PARALLELOGRAM } from './helper';

class Parallelogram {
  constructor(points) {
    this.coord = Parallelogram.getCoord(points);
    this.el = null;
    this._createElement();
  }
  _createElement() {
    this.el = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    this.el.setAttribute('stroke', PARALLELOGRAM.stroke.color.default);
    this.el.setAttribute('stroke-width', PARALLELOGRAM.stroke.width);
    this.el.setAttribute('fill', 'url(#linearGradient)');
    this._updateView();
  }
  _updateView() {
    this.el.setAttribute('points', this.coord);
  }
  update(points) {
    this.coord = Parallelogram.getCoord(points);
    this._updateView();
  }
  get element() {
    return this.el;
  }
  static getCoord(points) {
    return points.map(p => p.x + ',' + p.y).join(' ');
  }
}

export default Parallelogram;
